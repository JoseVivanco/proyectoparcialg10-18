/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import usuarios.*;
import java.util.*;
import static interfaz.Sistema.usuarioActivo;

/**
 *
 * @author Brank
 */
public class MenuTarjetahabiente extends Menu {

    public MenuTarjetahabiente(ArrayList<String> opciones) {
        super(opciones);
    }

    @Override
    public void cargarMenu() {
        boolean cerrarSesion = false;
        while (!(cerrarSesion)) {            
            mostrarMenu();
            System.out.print("Ingrese opción : ");
            String opcion = Util.ingresoString();
            while ((!(Util.isNumeric(opcion))) || (!(Util.isBetween(1, 4, opcion)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                opcion = Util.ingresoString();
            }
            switch (opcion) {
                case "1":
                    Tarjetahabiente.miCuenta((Tarjetahabiente) usuarioActivo);
                    break;
                case "2":
                    MenuEmpresas menuEmpresas = new MenuEmpresas(MenuEmpresas.añadirOpciones());
                    menuEmpresas.cargarMenu();
                    break;
                case "3":
                    MenuPromociones menuPromociones = new MenuPromociones(MenuPromociones.añadirOpciones());
                    menuPromociones.cargarMenu();
                    break;
                case "4":
                    cerrarSesion = true;
                    break;
            }
        }
    }

    public static ArrayList<String> añadirOpciones() {
        ArrayList<String> opciones = new ArrayList<>();
        opciones.add("Mi cuenta");
        opciones.add("Consultar empresas");
        opciones.add("Consultar promociones");
        opciones.add("Cerrar sesión");
        return opciones;
    }
}
