/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 *
 * @author Brank
 */
public class Util {

    public static String ingresoString() {
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }

    public static LocalDate ingresoFecha() {
        Scanner input = new Scanner(System.in);
        String fecha = input.nextLine();
        return toDate(fecha);
    }

    public static LocalTime ingresoHora() {
        Scanner input = new Scanner(System.in);
        String hora = input.nextLine();
        return toTime(hora);
    }

    public static LocalDate toDate(String s) {
        try {
            DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate horaLocalDate = LocalDate.parse(s, formatoFecha);
            return horaLocalDate;
        } catch (java.time.format.DateTimeParseException e) {
            return null;
        }
    }

    public static LocalTime toTime(String s) {
        try {
            DateTimeFormatter formatoHora = DateTimeFormatter.ofPattern("HH:mm");
            LocalTime horaLocalTime = LocalTime.parse(s, formatoHora);
            return horaLocalTime;
        } catch (java.time.format.DateTimeParseException e) {
            return null;
        }
    }
    
    public static String mostrarFecha(LocalDate fecha){
        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return fecha.format(formatoFecha);
    }

    public static boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException e) {
            resultado = false;
        }
        return resultado;
    }

    public static boolean isBetween(int minimo, int maximo, String n) {
        boolean resultado;
        try {
            Integer N = Integer.parseInt(n);
            if ((minimo <= N) && (N <= maximo)) {
                resultado = true;
            } else {
                resultado = false;
            }
        } catch (NumberFormatException e) {
            resultado = false;
        }
        return resultado;
    }

    public static String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static String toTitle(String s) {
        String retorno = "";
        String[] palabras = s.split(" ");
        for (String p : palabras) {
            retorno += capitalize(p);
            retorno += " ";
        }
        retorno = retorno.substring(0, retorno.length()-1);
        return retorno;
    }

    public static void limpiarPantalla() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
        }
    }

    public static void continuar() {
        System.out.println();
        System.out.println("Presione enter para continuar...");
        ingresoString();
    }

}
